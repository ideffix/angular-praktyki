import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabMenuModule, MenuItem } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';

import { AppComponent } from './app.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UserListComponent } from './user-list/user-list.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { GrowlModule } from 'primeng/primeng';
import { DataTableModule, SharedModule } from 'primeng/primeng';

import { UserServiceService } from './user-service.service';

@NgModule({
  declarations: [
    AppComponent,
    CreateUserComponent,
    UserListComponent,
    MainMenuComponent
  ],
  imports: [
    BrowserModule,
    TabMenuModule,
    ButtonModule,
    GrowlModule, 
    DataTableModule, 
    SharedModule,
    RouterModule.forRoot([
      {
        path: 'create',
        component: CreateUserComponent
      },
      {
        path: 'list',
        component: UserListComponent
      },
      {
        path: '',
        component: UserListComponent
      }
    ])
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
