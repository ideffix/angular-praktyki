export class User {

    name: string;
    age: Number;

    public constructor(name: string, age: Number) {
        this.name = name;
        this.age = age;
    }
}
