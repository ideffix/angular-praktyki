import { Injectable } from '@angular/core';
import { User } from './user';


@Injectable()
export class UserServiceService {

  users: Array<User> = [];

  constructor() { }

  addUser(user: User) {
    this.users.push(user);
  }

  showAllUsers() {
    return this.users;
  }

}
