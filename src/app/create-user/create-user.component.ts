import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { User } from '../user';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  
  msgs: Message[] = [];

  constructor(private userService: UserServiceService) { }

  ngOnInit() {
  }

  age: string;

  addUser(name: string, age: Number) {
    this.userService.addUser(new User(name, age));
    this.msgs = [];
    this.msgs.push({severity:'success', summary:'User added!', detail:'User ' + name + ' added to database'});
  }

}
