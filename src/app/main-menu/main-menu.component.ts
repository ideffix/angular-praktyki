import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/primeng';

@Component({
  selector: 'main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

  constructor() { }

  items: MenuItem[];

  ngOnInit() {
        this.items = [
            {label: 'Add user', icon: 'fa-user-plus', routerLink: ['/create']},
            {label: 'Show all users', icon: 'fa-reorder', routerLink: ['/list']}
        ];
  }

}
