import { PraktykiAppPage } from './app.po';

describe('praktyki-app App', () => {
  let page: PraktykiAppPage;

  beforeEach(() => {
    page = new PraktykiAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
